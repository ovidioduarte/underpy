#PRINCIPAL
import tkinter
from metodonumerico import Numerico
from ventilation import MineVentilator
from abalaubscher import AbaLaubscher

class Janela():

    
    def __init__(self):
        self._root = tkinter.Tk()
        w, h = self._root.winfo_screenwidth(), self._root.winfo_screenheight()
        self._root.geometry("%dx%d+0+0" % (w, h))
        self._root.update()
        h = int(self._root.winfo_height())
        
        self.frame1= tkinter.Frame(self._root,width=13, bg='blue')
        self.frame1.grid(row=0,column=0, ipady=h)
        self.frame2= tkinter.Frame(self._root)
        self.frame2.grid(row=0,column=1, rowspan=50,sticky='n')
        b1 = tkinter.Button(self.frame1, text='Metodo Numerico', font='arial 12 bold')
        b1.config(bg='mediumblue', fg='white', width=13, command=self.numerico)
        b1.config(activebackground='yellow', borderwidth=5, activeforeground='black')

        b2 = tkinter.Button(self.frame1, text='Metodo Empirico', font='arial 12 bold')
        b2.config(bg='red', fg='white', width=13, command=self.empirico)
        b2.config(activebackground='yellow', borderwidth=5, activeforeground='black')

        b3 = tkinter.Button(self.frame1, text='Ventilação', font='arial 12 bold')
        b3.config(bg='black', fg='white', width=13, command=self.ventilacao)
        b3.config(activebackground='yellow', borderwidth=5, activeforeground='black')

        b1.grid(row=1, column=0, columnspan=2, pady=4, padx=6, sticky='nw')
        b2.grid(row=2, column=0, columnspan=2, pady=4, padx=6, sticky='nw')
        b3.grid(row=3, column=0, columnspan=2, pady=4, padx=6, sticky='nw')
        b1.invoke()
        
        self._root.mainloop()
    
        
    def numerico(self):
        self.frame2.destroy()
        self.frame2= tkinter.Frame(self._root)
        self.frame2.grid(row=0,column=1, rowspan=50,sticky='n')
        self.frame1['bg'] = 'mediumblue'
        n = Numerico(self.frame2)
        n.grid(row=0, column=0, sticky='ne')

    def empirico(self):
        self.frame2.destroy()
        self.frame2= tkinter.Frame(self._root)
        self.frame2.grid(row=0,column=1, rowspan=50,sticky='n')
        self.frame1['bg'] = 'red'
        pg = AbaLaubscher(self.frame2)
        pg.grid(row=0, column=0)
        

    def ventilacao(self):
        self.frame2.destroy()
        self.frame2= tkinter.Frame(self._root)
        self.frame2.grid(row=0,column=1, rowspan=50,sticky='n')
        self.frame1['bg'] = 'black'
        mv = MineVentilator(self.frame2)
        
if __name__ == '__main__':
    j = Janela()

