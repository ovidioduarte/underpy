import tkinter
''' essa linha abaixo deve ser assim pois message box é um modulo de tkinter
não uma classe. Assim se ela nao for implicitamente importada haverá um erro
quando o programa for executado diretamente '''
from tkinter import messagebox
import math
from smartentry import SmartEntry

'''As 4 primeiras classes são para a exibição das 4 formulas e cálculo dos valores
que serão escolhidos pelo usuário. A estrutura destas 4 classes são iguais de forma que entendeu
uma as outras são fáceis de se entender'''
          
###################################################################################
'''Esta classe corresponde a classe que é invocada pela classe MineVentilator quando
o segundo radioButton da classe MineVentilator está selecionado'''

class Ventilacao1():

     def __init__(self, master, *args):
          self.superframe = tkinter.Frame(master)
          self.superframe.grid(row=0, column=0)

          self.frame1 = tkinter.Frame(self.superframe)
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame1.grid(row=0, column=0)
          self.frame2.grid(row=0, column=1)
          listaRadios = [] #lista que armazenará  os radiobuttons
          self.f1 = 'arial 12 bold'
          self.f2 = 'arial 11 bold'
          self.var = tkinter.IntVar()
          self.nomes = ['Perda de Carga ',
                        'Coeficiente de Fricção',
                        'Comprimento',
                        'Perímetro',
                        'Área Transversal',
                        'Densidade do ar',
                        'Velocidade'
                        ]
          l = tkinter.Label(self.frame1, text='Calcular:', font=self.f1)
          l.grid(row=0, column=0)

          for i in range(len(self.nomes)):
                r = tkinter.Radiobutton(self.frame1, text=self.nomes[i], value=i, variable=self.var)
                r.config(font=self.f1, command= self.criar)
                listaRadios.append(r)
                listaRadios[i].grid(row=i, column=1, sticky='nw')

          listaRadios[0].invoke()

     def listas(self):
          self.entries = []
          #limites das entries
          self.limites = [(0,1000,101000),   #perda de carga
                          (0.0035,0.05,0.108),          #não sei esses limites/ revisar depois
                          (1,100,1000),      #comprimento
                          (1,10,50),         #perímetro
                          (0.1,10,200),       #área
                          (1.12,1.3,1.43),   #densidades do ar min, normal, máx
                          (0.1,11,20)         #velocidade
                          ]
         
          self.unidades = ['Pa',
                           '',
                           'm',
                           'm',
                           'm²',
                           'kg/m³',
                           'm/s'
                           ]
          

     def criar(self):
          self.frame2.destroy()
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1, column=0)
          self.listas()
          x = self.var.get() #qual é o valor do radioButton selecionado
          index = 0
          for i in range(len(self.nomes)):
               if i == x:
                    pass

               else:#esses 3 if's são pra não exibir o parenteses na label do coeficiente
                    if (x == 0) and i == 1:
                         texto = self.nomes[1] + self.unidades[1] + ':'

                    elif x > 1 and i == 1:
                         texto = self.nomes[1] + self.unidades[1] + ':'

                    else:
                         texto = self.nomes[i] + ' (' + self.unidades[i] + '):'
                         
                    
                    
                    l = tkinter.Label(self.frame2, text=texto, font=self.f2)
                    l.grid(row=index, column=0, stick='ne', ipadx=2)

                    e = SmartEntry(self.frame2, self.limites[i][1], self.limites[i][0], self.limites[i][2])
                    e.config(fg='blue', font=self.f1, justify='center')
                    e.config(highlightcolor='blue', borderwidth=4, highlightthickness=3)
                    self.entries.append(e)
                    self.entries[index].grid(row=index, column=1, sticky='nw')
                    index +=1

          l = tkinter.Label(self.frame2, text=(self.nomes[x]+':'), font=self.f1)
          l.grid(row=6, column=0, sticky='ne', ipadx=2)
          self.lresultado = tkinter.Label(self.frame2, text='', font=self.f1)
          self.lresultado.grid(row=6, column=1, sticky='nw')
          bcalcula = tkinter.Button(self.frame2, text='Calcular', bg='orangered', fg='white', command= self.calcula)
          bcalcula.config(borderwidth=5, activebackground='lawngreen', activeforeground='black', font=self.f1)
          bcalcula.grid(row=7, column=1)

     def validar(self, lista):
          for l in lista:
               if l.get() is False:
                    texto = 'Um dos seguintes erros ocorreu:\n\n1) Caracter inválido nos campos de entrada'
                    texto += '\n2) Valores acima ou abaixo do permitido nos campos de entrada\n\n'
                    texto += 'Corrija os campos de entrada que estiverem destacados em vermelho !!!'
                    m = messagebox.showerror('ERRO', texto)
                    return False
          return True

     def calcula(self):
          if self.validar(self.entries) is False:
               return False

          else:
               x = self.var.get() #valor que o radiobutton retorna
               if x == 0:
                    resultado = self.entries[0].get()*self.entries[1].get()*self.entries[2].get()
                    resultado = (resultado/self.entries[3].get()) * self.entries[4].get()*((self.entries[5].get()**2)/2)
                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + ' ' + self.unidades[x]
                    
               elif (x > 0 and x < 4) or x == 5:
                    #isso dara sempre pressão * 2* area dividido por algo
                    resultado = self.entries[0].get()* 2 *self.entries[3].get()/self.entries[1].get() 
                    resultado /= (self.entries[2].get()*self.entries[4].get()*(self.entries[5].get()**2))

                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + ' ' + self.unidades[x]

               elif x == 4:
                    resultado = self.entries[1].get()* self.entries[2].get()* self.entries[3].get()
                    resultado *= self.entries[4].get()* (self.entries[5].get()**2)
                    resultado /= ( self.entries[0].get()* 2)
                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + ' ' + self.unidades[x]

               elif x == 6:
                    resultado = self.entries[0].get() * 2 * self.entries[4].get()
                    resultado /=(self.entries[1].get()*self.entries[2].get()*self.entries[3].get()*self.entries[5].get())
                    
                    resultado = resultado **(1/2)
                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + ' ' + self.unidades[x]
                    
          

class Ventilacao2():

     def __init__(self, master, *args):
          self.superframe = tkinter.Frame(master)
          self.superframe.grid(row=0, column=0)

          self.frame1 = tkinter.Frame(self.superframe)
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame1.grid(row=0, column=0)
          self.frame2.grid(row=0, column=1)
          listaRadios = []
          self.var = tkinter.IntVar()
          self.nomes =['Perda de Carga ',
                   'Fator de Fricção',
                   'Comprimento',
                   'Perímetro',
                   'Área Transversal',
                   'Velocidade'
                   ]
          lCalcula = tkinter.Label(self.frame1, text='Calcular:', font='arial 12 bold')
          lCalcula.grid(row=0, column=0)
          for i in range(len(self.nomes)):
               r = tkinter.Radiobutton(self.frame1, text=self.nomes[i], value=i, variable=self.var)
               r.config(font='arial 12 bold', command=self.criarCampos)
               listaRadios.append(r)
               listaRadios[i].grid(row=i, column=1, sticky='nw')
               
          listaRadios[0].invoke()

     def listas(self):
          self.entries = []
          #limites das entries
          self.limites = [(0,1000,101000),   #perda de carga
                          (0.002,0.007,0.019),          #coef. fricção
                          (1,100,1000),      #comprimento
                          (1,10,50),         #perímetro
                          (0.1,10,200),       #área
                          (0.1,3,20)         #velocidade
                          ]
          
          self.texto = ['Perda de carga (Pa):',
                     'Fator de Fricção (kg/m³):',
                     'Comprimento (m):',
                     'Perímetro (m):',
                     'Área Transversal (m²):',
                     'Velocidade (m/s):'
                        ]
          self.unidades = [' Pa',
                           ' kg/m³',
                           ' m',
                           ' m',
                           ' m²',
                           ' m/s'
                           ]
          
          self.f1 = 'arial 12 bold'
          self.f2 = 'arial 11 bold'
          

     def criarCampos(self):
          self.frame2.destroy()
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1, column=0)
          self.listas()
          x = self.var.get() #qual é o valor do radioButton selecionado
          index = 0
          for i in range(len(self.texto)):
               if i == x:
                    pass

               else:
                    l = tkinter.Label(self.frame2, text=self.texto[i], font=self.f2)
                    l.grid(row=index, column=0, stick='ne', ipadx=2)

                    e = SmartEntry(self.frame2, self.limites[i][1], self.limites[i][0], self.limites[i][2])
                    e.config(fg='blue', font=self.f1, justify='center')
                    e.config(highlightcolor='blue', borderwidth=4, highlightthickness=3)
                    self.entries.append(e)
                    self.entries[index].grid(row=index, column=1, sticky='nw')
                    index +=1

          l = tkinter.Label(self.frame2, text=(self.nomes[x]+':'), font=self.f1)
          l.grid(row=6, column=0, sticky='ne', ipadx=2)
          self.lresultado = tkinter.Label(self.frame2, text='', font=self.f1)
          self.lresultado.grid(row=6, column=1, sticky='nw')
          bcalcula = tkinter.Button(self.frame2, text='Calcular', bg='darkorchid4', fg='white', command= self.calcula)
          bcalcula.config(borderwidth=5, activebackground='deepskyblue1', activeforeground='black', font=self.f1)
          bcalcula.grid(row=7, column=1)


     def validar(self, lista):
          i = 0
          for l in lista:
               if l.get() is False:
                    texto = 'Um dos seguintes erros ocorreu:\n'
                    texto += '\n1) Campo preenchido com valor acima ou abaixo do permitido;\n'
                    texto += '2) Um caracter inválido foi digitado em algum campo de entrada'
                    texto += '\n\nCorrija os campos destacados em vermelho !!!'
                    m = messagebox.showerror('ERRO', texto)
                    return False
                                                              
          return True
     
     def calcula(self):
          if self.validar(self.entries) is False:
               return False

          else:
               # x é o radio button selecionado e baseado no valor de x
               #uma determinada formula será usada
               x = self.var.get()
               if x == 0:
                    resultado = self.entries[0].get() * self.entries[1].get()
                    resultado *= (self.entries[2].get()/self.entries[3].get())*(self.entries[4].get()**2)
                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + self.unidades[x]

               elif x > 0 and x< 4:
                    resultado = self.entries[0].get() * self.entries[3].get() #pressao*area
                    resultado /= (self.entries[1].get() * self.entries[2].get() *(self.entries[4].get()**2))
                    resultado = round(resultado,3)
                    self.lresultado['text']= str(resultado) + self.unidades[x]

               elif x == 4:
                    resultado = self.entries[0].get()  #pressao
                    resultado /= (self.entries[1].get() * self.entries[2].get()*self.entries[3].get() *(self.entries[4].get()**2))
                    resultado = round(resultado,3)
                    self.lresultado['text']= str(resultado) + self.unidades[x]

               elif x ==5:
                    resultado = self.entries[0].get()* self.entries[4].get() #pressao*area
                    resultado /= (self.entries[1].get() * self.entries[2].get()*self.entries[3].get())
                    resultado = resultado **(0.5)
                    resultado = round(resultado,3)
                    self.lresultado['text']= str(resultado) + self.unidades[x]
          
     
######################################################################
class Ventilacao3():

     def __init__(self, master, *args):
          self.superframe=tkinter.Frame(master)
          self.superframe.grid(row=0, column=0)

          self.frame1 = tkinter.Frame(self.superframe)
          self.frame1.grid(row=0,column=0)
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=0,column=1)

          l= tkinter.Label(self.frame1, text='Calcular:', font='arial 12 bold')
          l.grid(row=0,column=0)
          self.lista = ['Perda de carga',
                   'Fator de Fricção',
                   'Comprimento',
                   'Perímetro',
                   'Área Transversal',
                   'Vazão'
                   ]
          self.var = tkinter.IntVar()#variavel que rastreará qual radio button esta ativo
          listaRadio = []
          for i in range(len(self.lista)):
               r = tkinter.Radiobutton(self.frame1, text=self.lista[i], value=i, variable=self.var)
               r.config(font='arial 12 bold',  command=self.criarCampos)
               listaRadio.append(r)
               listaRadio[i].grid(row=i, column=1, sticky='w')
          
          #self.var.trace('w',self.criar)
          self.criaListas()
          listaRadio[0].invoke()
          
     def criaListas(self):
          self.listaEntry = []
          self.f1 = 'arial 12 bold'
          self.f2 = 'arial 11 bold'
          
          self.texto = ['Perda de carga (Pa):',
                     'Fator de Fricção (kg/m³):',
                     'Comprimento (m):',
                     'Perímetro (m):',
                     'Área Transversal (m²):',
                     'Vazão (m³/s):'
                        ]
          self.limites = [(0,999,1000000),
                          (0.002,0.05,0.1),
                          (1,100,1000),
                          (1,10,50),
                          (0.1,5,200),
                          (0.01,15,200)]
          

     def criarCampos(self):
          self.frame2.destroy()
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1,column=0)
          self.criaListas()
          x = self.var.get()
          iten=0
          for i in range(6):               
               if i == x:
                    pass
               
               else:
                    l = tkinter.Label(self.frame2, text= self.texto[i], font=self.f2)
                    l.grid(row=iten, column=0, sticky='ne')
                    
                    e = SmartEntry(self.frame2, self.limites[i][1],self.limites[i][0],self.limites[i][2])
                    e.configure(justify='center', fg='blue', borderwidth=4)
                    e.configure(highlightthickness=3, highlightcolor='blue', font=self.f2)
                    self.listaEntry.append(e)
                    self.listaEntry[iten].grid(row=iten, column=1, stick='nw')
                    iten+=1

          l = tkinter.Label(self.frame2, text=(self.lista[x]+':'), font=self.f1)
          l.grid(row=6, column=0, sticky='ne', ipadx=2)
          self.lresultado = tkinter.Label(self.frame2, text='', font=self.f1)
          self.lresultado.grid(row=6, column=1, sticky='nw')
          bcalcula = tkinter.Button(self.frame2, text='Calcular', bg='black', fg='white')
          bcalcula.config(borderwidth=5, activebackground='white', activeforeground='black', font=self.f1)
          bcalcula.grid(row=7, column=1)

          if x == 0:
               bcalcula['command'] = self.calculaPerda

          elif x == 1:
               bcalcula['command'] = self.calculaFriccao
               
          elif x == 2:
               bcalcula['command'] = self.calculaComprimento
               
          elif x == 3:
               bcalcula['command'] = self.calculaPerimetro
               
          elif x == 4:
               bcalcula['command'] = self.calculaArea
               
          elif x == 5:
               bcalcula['command'] = self.calculaVazao
          

     def validar(self, lista):
          for l in lista:
               if l.get() is False:
                    texto = 'Nos campos em vermelho um dos seguintes erros ocorreu:\n'
                    texto += '\n1) Campo preenchido com valor acima ou abaixo do permitido;\n'
                    texto += '2) Um caracter inválido foi digitado em algum campo de entrada'
                    texto += '\n\nCorrija todos os campos que estiverem destacados !!!'
                    m = messagebox.showerror('ERRO', texto)
                    return False
          return True
     
      
     def calculaPerda(self, *args):
          a = self.validar(self.listaEntry)
          if a is False:
               pass

          else:
               k = self.listaEntry[0].get()
               L = self.listaEntry[1].get()
               per = self.listaEntry[2].get()
               A3 = self.listaEntry[3].get()**3
               Q2 = self.listaEntry[4].get()**2
               
               resultado = k * L * (per/A3) * Q2
               self.lresultado['text'] = str(round(resultado,4)) + ' Pa'


     def calculaFriccao(self, *args):
          a = self.validar(self.listaEntry)
          if a is False:
               return False
          
          else:
               for i in range(1,5):
                    if i == 3:
                         pass
                    
                    elif self.listaEntry[i].get()==0:
                         texto = ('O valor dos campos Comprimento, Perímetro, Área e Vazão não podem ser iguais a zero\n\n')
                         texto += 'Se algum destes campos for zero corrija-os !!!!'
                         m = messagebox.showerror('DIVISÃO POR ZERO', texto)
                         return False
                         
               p = self.listaEntry[0].get()
               L = self.listaEntry[1].get()
               per = self.listaEntry[2].get()
               A3 = self.listaEntry[3].get()**3
               Q2 = self.listaEntry[4].get()**2
               
               resultado = p/ ( L * (per/A3) * Q2)
               self.lresultado['text'] = str(round(resultado,4)) + ' kg/m³'

     
     def calculaComprimento(self):
          if self.validar(self.listaEntry) is False:
               return False

          else:
               for i in range(1,len(self.listaEntry)):
                    if self.listaEntry[i].get() == 0:
                         texto = 'Alguns dos seguintes campos é igual a zero\n'
                         texto = texto +'1) Fator de Fricção\n2) Perímetro\n3) Área\n4) Vazão'
                         texto += '\n\nCorrija qual destes campos contiver o número 0 !!'
                         m = messagebox.showerror('DIVISAO POR ZERO', texto)
                         return False

               #resultado = Fator * (perimetro/(area^3))
               resultado = self.listaEntry[1].get() * (self.listaEntry[2].get()/(self.listaEntry[3].get()**3))
               
               #resultado = (Perda de pressa)/ resultado * vazao^2
               resultado = self.listaEntry[0].get()/ (resultado * (self.listaEntry[4].get()**2))
               resultado = round(resultado,4)
               self.lresultado['text'] = str(resultado) + ' m'

     
     def calculaPerimetro(self):
          if self.validar(self.listaEntry) is False:
               return False
          else:
               for i in range(1,len(self.listaEntry)):
                    if self.listaEntry[i].get() == 0:
                         texto = 'Alguns dos seguintes campos é igual a zero\n'
                         texto = texto +'1) Fator de Fricção\n2) Comprimento\n3) Área\n4) Vazão'
                         texto += '\n\nCorrija os campos que estiverem em vermelho !!'
                         m = messagebox.showerror('DIVISAO POR ZERO', texto)
                         return False

               #resultado = pressao * (area^3)
               resultado = self.listaEntry[0].get() * (self.listaEntry[3].get()**3)
               #resultado = resultado)/ k * comprimento * vazao^2
               resultado = resultado/(self.listaEntry[1].get()*self.listaEntry[2].get()*(self.listaEntry[4].get()**2))
               resultado = round(resultado,4)
               self.lresultado['text'] = str(resultado) + ' m'

     def calculaArea(self):
          if self.validar(self.listaEntry) is False:
               return False

          else:
               if self.listaEntry[0].get() == 0:
                    texto = 'O campo referente a Perda de Pressão não pode ser 0.\n'
                    texto = texto + '\nCorrija esse esse campo !!!!'
                    m = messagebox.showerror('DIVISÃO POR ZERO', texto)
                    return False
               
               else:
                    resultado = self.listaEntry[1].get()*self.listaEntry[2].get()
                    resultado = resultado * self.listaEntry[3].get()* ((self.listaEntry[4].get())**2)

                    resultado = resultado/self.listaEntry[0].get()
                    resultado = (resultado ** (1/3))
                    resultado = round(resultado,3)
                    self.lresultado['text'] = str(resultado) + ' m²'
               
     def calculaVazao(self):
          if self.validar(self.listaEntry) is False:
               return False

          else:
               for i in range(4):
                    if self.listaEntry[i].get() == 0:
                         texto = 'Um dos seguintes valores é igual a zero\n1) Fator de Fricção\n2) Comprimento\n'
                         texto += '3) Perímetro\n3) Área\n\n Corrija os que estiverem preenchidos em vermelho !!!'
                         m = messagebox.showerror('DIVISÃO POR ZERO', texto)
                         return False
               resultado = self.listaEntry[0].get()*(self.listaEntry[4].get()**3)
               resultado /= self.listaEntry[1].get()*self.listaEntry[2].get()*self.listaEntry[3].get()
               resultado = resultado ** (1/2)
               resultado = round(resultado,3)
               self.lresultado['text'] = str(resultado) + ' m³/s'
               
    

          
#####################################################################################
'''Esta classe corresponde a classe que é invocada pela classe MineVentilator quando
o 4° radioButton está selecionado '''

class Ventilacao4():

     def __init__(self, master, *args):
          #utilizei um superframe e dois frames para evitar ter que passar parametros
          #toda hora para os métodos da classe
          self.superframe=tkinter.Frame(master)
          self.superframe.grid(row=0, column=0)

          self.frame1 = tkinter.Frame(self.superframe)
          self.frame1.grid(row=0,column=0)
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=0,column=1)

          l= tkinter.Label(self.frame1, text='Calcular:', font='arial 12 bold')
          l.grid(row=0,column=0)
          self.campos =['Perda de Carga','Resistência', 'Vazão']
          self.listaRadio = []
          self.var = tkinter.IntVar()
          for i in range(len(self.campos)):
               r = tkinter.Radiobutton(self.frame1,text=self.campos[i], value=i, variable=self.var)
               r.config( font='arial 12 bold', command=self.criaCampos)
               self.listaRadio.append(r)
               self.listaRadio[i].grid(row=i, column=1, sticky='nw')
                    
          self.listaRadio[0].invoke()
          

     def criaListas(self):
          self.limites = [(0,100,101000),(0.002,0.05,0.1),(0.01,5,20)] #limites de valores
          self.unidades = [' Pa', ' kg/m³', ' m³/s']
          self.elista = []
         
          self.f1 = 'arial 12 bold'
          self.f2 = 'arial 11 bold'
          

     def criaCampos(self, *args):
          self.frame2.destroy()
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1, column=0)
          self.criaListas()
          x = self.var.get()
          listaLabels = []
          itens = 0        
          for i in range(len(self.listaRadio)):
               if i == x:
                    pass

               else:
                    texto = self.campos[i]+ ' (' + self.unidades[i] + '):'
                    l = tkinter.Label(self.frame2, text= texto, font=self.f2)
                    listaLabels.append(l)
                    listaLabels[itens].grid(row=itens, column=0, sticky='ne')

                    e = SmartEntry(self.frame2, self.limites[i][1], self.limites[i][0],self.limites[i][2])
                    e.configure(justify='center', fg='blue', borderwidth=3)
                    e.configure(highlightthickness=3, highlightcolor='blue', font=self.f1)
                    self.elista.append(e)
                    self.elista[itens].grid(row=itens, column=1, sticky='nw')
                    itens += 1 

          ltexto = tkinter.Label(self.frame2, text= (self.campos[x] + ':'), font=self.f1)
          ltexto.grid(row=3, column=0, sticky='ne')
          self.lresultado = tkinter.Label(self.frame2, text='', font=self.f1)
          self.lresultado.grid(row=3, column=1, sticky='nw')
          bcalcula = tkinter.Button(self.frame2, text='Calcular', font=self.f1)
          bcalcula.config(bg='mediumblue', fg='white', command=self.calcula)
          bcalcula.config(activebackground='yellow', borderwidth=5, activeforeground='black')
          bcalcula.grid(row=4, column=1, columnspan=2)
          bcalcula.bind('<Return>',self.calcula)#associa a tecla enter com a função calcula


     def validar(self, lista):
          for l in lista:
               if l.get() is False:
                    texto = 'Nos campos em vermelho um dos seguintes erros ocorreu:\n1) Campo preenchido com valor acima ou abaixo do permitido;\n'
                    texto2 = '2) Um caracter inválido foi digitado \n\nCorrija todos os campos que estiverem destacados !!!'
                    m = messagebox.showerror('ERRO', (texto+texto2))
                    return False
          return True

     def calcula(self, *args):
          if self.validar(self.elista) is False:
               return False

          else:
               x = self.var.get()
               if x == 0:
                    perdaPressao = self.elista[0].get() * (self.elista[1].get()**2)
                    perdaPressao = round(perdaPressao,3)
                    self.lresultado['text'] = str(perdaPressao) + self.unidades[x]

               elif x == 1:
                    resistencia = self.elista[0].get()/(self.elista[1].get() ** 2)
                    resistencia = round(resistencia,4)
                    self.lresultado['text'] = str(resistencia) + self.unidades[x]

               elif x == 2:                    
                    vazao = self.elista[0].get()/ self.elista[1].get()
                    vazao = math.sqrt(vazao)
                    vazao = round(vazao,3)
                    self.lresultado['text'] =str(vazao) + self.unidades[x]          
          


''' CLASSE PRINCIPAL
     Essa classe criará 4 instancia das 4 classes acima. Qual instância
     será criada dependerá de qual rádioButton está selecionado sendo que
     radioButton corresponde a uma instancia de uma das 4 classes acima'''

class MineVentilator():
     

     def __init__(self, master):
          self.superframe=tkinter.Frame(master)
          self.superframe.grid(row=0, column=0)

          self.frame1 = tkinter.Frame(self.superframe)
          self.frame1.grid(row=0,column=0, pady=15)
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1,column=0)

          ltexto1 = tkinter.Label(self.frame1, text='Fórmula escolhida:', font='arial 12 bold')
          ltexto1.grid(row=0, column=0, sticky='w',padx=3, pady=5)
          f = 'arial 12 bold'
          
          texto = ['1) p = (f * L) * (per / A) * d * (v²/2)',
                   '2) p = k * L *(per / A) * v²',
                   '3) p = k * L *(per / A³) * Q²',
                   '4) p = R * Q²'
                   ]

          lRadio = []
          self.var = tkinter.IntVar()
          for i in range(4):
               r = tkinter.Radiobutton(self.frame1, font=f, text=texto[i], value=i, variable=self.var, command=self.exibe)
               lRadio.append(r)
               lRadio[i].grid(row=i, column=1, sticky='w', padx=10, pady=3)

          lRadio[0].invoke() #para algum radioButton estar ativo deve-se invocar algum deles
          
          
     def exibe(self, *args):
          self.frame2.destroy()
          self.frame2 = tkinter.Frame(self.superframe)
          self.frame2.grid(row=1,column=0)
          
          if self.var.get() == 0:
               v = Ventilacao1(self.frame2)

          elif self.var.get() == 1:          
               v = Ventilacao2(self.frame2)

          elif self.var.get() == 2:
               v = Ventilacao3(self.frame2)

          elif self.var.get() == 3:
               v = Ventilacao4(self.frame2)



if __name__== '__main__':
     root = tkinter.Tk()
     root.geometry('600x800')
     mv = MineVentilator(root)
     root.mainloop()
