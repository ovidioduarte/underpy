import tkinter as tk

class LaubscherGrafico(tk.Frame):  # Inheritance
    def __init__(self, parent, **kw): 
        tk.Frame.__init__(self, parent, **kw)  # inicializando o frame

        # métodos da classe LaubscherGrafico
        self.canvas()
        self.linhasVerticais()
        self.linhasHorizontais()
        self.criaMRMR()
        
 

    def canvas(self):
        # fundo que abrigará o gráfico
        self.fundo = tk.Canvas(self, width=1000, height=700, bg='black')
        self.fundo.grid(row=0, column=0, rowspan=10)

        # gráfico propriamente dito
        '''Ha um espacamento nos grafico, ver canto inferior, como eliminar?'''
        self.chart = tk.Canvas(self, width=900, height=650, bg='snow4',
                               highlightthickness=0, highlightbackground='white')
        
        self.chart['cursor'] = 'cross'
        self.chart.grid(row=0, column=0, rowspan=10)

    def linhasVerticais(self):

        # largura e altura do grafico
        lc = float(self.chart['width'])
        hc = float(self.chart['height'])

        # largura e altura do fundo
        lf = float(self.fundo['width'])
        hf = float(self.fundo['height'])

        # posições verticais entre grafico e o fundo
        py = (hf - hc) / 2 + 12
        px = (lf - lc) / 2

        # dicionario com as proporções baseadas no gráfico original
        
        df = 3.95  # distância entre do 1 valor até o último
        self.proporcao = {0:0,
                       1:(1.2 / df),
                       2:(1.9 / df),
                       3:(2.4 / df),
                       4:(2.8 / df),
                       5:(3.1 / df),
                       6:(3.4 / df),
                       7:(3.6 / df),
                       8:(3.77 / df)
                       }
        f = 'arial 13 bold'

        for k, v in self.proporcao.items():
            # posição dos 3 grupos de linhas
            p1 = v * (lc / 3)
            p2 = p1 + (lc / 3)
            p3 = p2 + (lc / 3)

            '''
            A linha de 0.1 não se destada, por que ????
            Dar uma olhada na estetica do gráfico, falta alguns ajustes
            '''           
            l1 = self.chart.create_line(p1, 0, p1, hc, activefill='magenta', width=3)
            if(k != 7):
                n1 = self.fundo.create_text(px + p1, py + hc, text=('.' + str(k + 1)),
                                            font=f, fill='white', activefill='magenta')

            l2 = self.chart.create_line(p2, 0, p2, hc, activefill='magenta', width=3)
            n2 = self.fundo.create_text(px + p2, py + hc, text=(str(k + 1)),
                                        font=f, fill='white', activefill='magenta')

            l3 = self.chart.create_line(p3, 0, p3, hc, activefill='magenta', width=3)
            if(k == 7):
                continue
            n3 = self.fundo.create_text(px + p3, py + hc, text=(str(10 * (k + 1))),
                                        font=f, fill='white', activefill='magenta')

    def linhasHorizontais(self):
        
        hc = float(self.chart['height'])
        lc = float(self.chart['width'])
        dv = (float(self.fundo['height']) - hc) / 2
        
        for i in range(10):
            # retorna o valor dos angulos... 20°, 30°, etc
            texto = (str(i * 10) + '°')

            # posição y das linhas horizontais
            y_linhas = (9 - i) * (hc / 9)

            # posição do texto que será exibido do lado das linhas
            y_texto = y_linhas + dv

            horizontal = self.chart.create_line(0, y_linhas, lc, y_linhas, activefill='red', width=3)

            angulo = self.fundo.create_text(20, y_texto, text=texto, fill='white',
                                                 font='arial 14 bold', activefill='red')
    def criaMRMR(self):
        """Desenhando linhas diagonais"""
        # criando as linhas inclinadas
        
        mrmr = self.chart.create_text(35, 30, text='MRMR', font='arial 14 bold',
                                      fill='blue', activefill='green1')
        
        lc = float(self.chart['width'])
        hc = float(self.chart['height'])
        razao = hc / 9  # altura do gráfico dividido por 9

        '''
        Valores de y final foram feitos usando proporções observadas no
        desenho original. Para o caso da linha de 20 que se encontra a 6 razões
        ou partes abaixo do inicio de y e a 66% da distância entre os ângulos de
        30° e 20°
        A logica usada foi: y_20 == 6 + 0.666 *(razao)
        Para o valor de 10 foi usado um valor de 0.5263
        '''
        
        # os valores de MRMR de 20 e 10 são exceções entao serão criados separadamente.
        # retirei o self de todas as linhas do mrmr e dos textos correspondentes a elas

        linha_20 = self.chart.create_line(lc, 0, 0, (6.666 * razao), fill='blue',
                                               width=3, activefill='green1')
        mrmr_20 = self.chart.create_text(30, (6.666 * razao - 25), font='arial 14 bold',
                                              text='20', fill='blue', activefill='green1')
        
        linha_10 = self.chart.create_line(lc, 0, 0, (7.5263 * razao), fill='blue',
                                               width=3, activefill='green1')
        mrmr_10 = self.chart.create_text(30, (7.5263 * razao - 25), font='arial 14 bold',
                                              text='10', fill='blue', activefill='green1')
        
        # Criação das linhas e textos restantes
        for i in range(3, 9):
                 
            y = (9 - i) * razao  # coordenada y final das linhas
            mrmr = self.chart.create_line(lc, 0, 0, y, fill='blue', width=3, activefill='green1')
            
            texto_mrmr = self.chart.create_text(30, (y - 25), text=str(i * 10),
                                                   font='arial 14 bold', fill='blue', activefill='green1')


if __name__ == '__main__':
    root = tk.Tk()
    root.title('executando como __main__')
    
    a = LaubscherGrafico(root)
    a.pack()

    # #DUVIDA
    # é possivel lancar duas instancias em uma mesma janela pai
    # mas para faze-las preciso, definir o posicionamento delas na janela
    # como fazer isso?
    outra_janela = tk.Toplevel()
    outra_janela.title('Outra')
    b = LaubscherGrafico(outra_janela)
    b.pack()
    root.mainloop()
