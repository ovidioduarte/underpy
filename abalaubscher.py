import tkinter as tk
from laubscherform import LaubscherForm
from laubschergrafico import LaubscherGrafico

class AbaLaubscher(tk.Frame):
    
    def __init__(self, master, **kw):
        tk.Frame.__init__(self, master, **kw)
        #self['bg']= 'gray'
        self.form = LaubscherForm(self)
        self.form.grid(row=0, column=0, sticky='s', pady=3)

        buttonPlotar = tk.Button(self, text= 'Plotar no gráfico', bg= 'mediumblue', command=self.plotaLinhas)
        buttonPlotar.config( relief='raised', fg='white', bd=5, font='arial 11 bold')
        buttonPlotar.grid(row=1, column=0, sticky='ne')
        
        self.grafico = LaubscherGrafico(self)
        self.grafico.grid(row=0, column=1, rowspan=10, sticky='nw', padx=9)

        self.width = float(self.grafico.chart['width'])/3
        self.height = float(self.grafico.chart['height'])/9
        self.rastreador = True    

    def yMRMR(self):
        mrmr = self.form.entries[4].get()
        xf = 3 * self.width #largura do grafico /3
        #quando se digita um valor incorreto ainda aparece erro no interpretador
        try:
            if mrmr > 30:
                mult = ((90 - mrmr)/10) #4
                #nao mexa nessa linha de baixo, ta certo
                #tem erro pra quando os valores sao redondo ... 50, 40
                py =  mult * self.height
                return py
                
            if mrmr <= 30 and mrmr >= 20:
                #a linha de 20 esta a 66 % da distancia entre as linhas de 20 e 30
                py = (((30 - mrmr)/10)* self.height * 0.666) + (6 * self.height)
                return py
            
            if mrmr < 20 and mrmr >= 10:
                r = 7.5 - 6.66
                py = (((20 - mrmr)/10) * r * self.height ) + (6.66 * self.height)
                return py
        except:
            pass
            
        
    def xFator(self):
        self.yMRMR()
        fator = self.form.calcularFator()
        if fator is False:
            return

        else:    
            px = 0
            
            if fator < 1:
                limiteMin = 0.1
                limiteMax = 0.2
                for i in range(0,10, 1):
                    if fator >= limiteMin and fator <= limiteMax:
                        if i < 8:
                            #deslocamento x da linha
                            dx = ((fator - limiteMin)/0.1) * (self.grafico.proporcao[i+1] - self.grafico.proporcao[i])
                            px = (self.width) * ((self.grafico.proporcao[i]) + dx)
                            return px
                            break
                        
                        else:                  
                            dx = ((fator - limiteMin)/0.1) * (1 - self.grafico.proporcao[8])
                            px = (self.width) * ((self.grafico.proporcao[8]) + dx )
                            return px
                            break
                    
                    else:
                        limiteMin = limiteMax
                        limiteMax = limiteMax + 0.1
                        
            if (fator >= 1) and (fator < 10):        
                limiteMin = int(fator)
                limiteMax = int(fator + 1)
                i = int(fator-1)
                
                if i < 8: #7.5
                    
                    dx = (fator - limiteMin) * (self.grafico.proporcao[i+1] - self.grafico.proporcao[i])            
                    px = self.width + (self.width * ((self.grafico.proporcao[i]) + dx))
                    return px
                    
                else:
                    dx = (fator - limiteMin) * (1 - self.grafico.proporcao[8])
                    px = (self.width * ((self.grafico.proporcao[8])+ dx))
                    px = px + self.width
                    return px
                                  
            if (fator >= 10) and (fator <= 100):
                limiteMin = (int(fator/10)*10) #20
                limiteMax = limiteMin + 10 #0.3
                i = int((fator/10))-1
                
                if i < 8: 
                    dx = ((fator - limiteMin)/10) * (self.grafico.proporcao[i+1] - self.grafico.proporcao[i])
                    px =  (self.width * ((self.grafico.proporcao[i]) + dx))
                    px = px + (2 * self.width)
                    return px
                    
                else:
                    dx = ((fator - limiteMin)/10) * (1 - self.grafico.proporcao[8])
                    px = (self.width * ((self.grafico.proporcao[8])+ dx))
                    px = px + (2 * self.width)
                    return px
                
    def apagaLinhas(self):
       if self.rastreador is False:
           self.grafico.laubschergrafico.delete(self.linhaFator)
           self.grafico.laubschergrafico.delete(self.linhaMRMR)
           self.grafico.laubschergrafico.delete(self.linhaSubsidencia)
           

    def plotaLinhas(self):  
        #coordenada x do fator
        xfator = self.xFator()
        
        #Esse if verifica se verifica se a erro no campo de entradas
        #pois o metodo self.xFator() invoca o metodo calcularFator() da instancia form
        #e esse metodo vverifica e valida todos os valores das entradas
        if xfator is None:
            pass
        else:
            inclinacao = (self.yMRMR()) /(3 * self.width)
            
            xF = (3*self.width) - xfator
            yF = ((3*self.width) - xfator) * (inclinacao)

            #apaga as linhas que ja foram criadas
            self.apagaLinhas()
            
            #linha do fator
            self.linhaFator = self.grafico.laubschergrafico.create_line(xfator, (9*self.height), xfator, yF, fill='hotpink', dash=(60,10),width=3)
            self.linhaMRMR = self.grafico.laubschergrafico.create_line((3*self.width), 0, xfator, yF, fill='yellow', width=3, dash=(60,40))
            self.linhaSubsidencia = self.grafico.laubschergrafico.create_line(xfator, yF, 0, yF, fill='orange', dash=(60,10),width=3)
            self.angulo = ((float(self.grafico.laubschergrafico['height']))-yF)/float(self.grafico.laubschergrafico['height'])
            self.angulo = self.angulo * 90
            
            self.rastreador = False
            self.valorAngulo()

#exibir o valor do angulo em uma label
    def valorAngulo(self):
        if self.rastreador is True:
            self.form.l2['text'] = ''

        if self.rastreador is False:
            self.form.l2['text'] = ''
            self.form.l2['text'] = str(round(self.angulo,3)) + ' graus'
            

if __name__ == '__main__':
    root = tk.Tk()
    root.title('PySubsidencia')
    criar = AbaLaubscher(root)
    criar.grid(row=0, column=0)
    
    root.mainloop()
