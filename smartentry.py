import tkinter as tk

class SmartEntry(tk.Entry):
    
    def __init__(self, master, initialVal=50, minVal=0, maxVal=100, **kw):
        
        #invoca o método construtor de uma entry e a armazena em master
        #basicamente o que ele diz pro interpretador é:
        ''' Crie uma entry, porém nos vamos modifica-la depois'''
        tk.Entry.__init__(self, master, **kw)
        
        #cria uma variavel para o tkinter
        self.var = tk.DoubleVar()
        self['textvariable'] = self.var

        #revisar isso aqui,
        #excelente explicação no effbot
        self.var.trace('w', self.validar)
        
        self.setMin(minVal)
        self.setMax(maxVal)
        
        self.var.set(initialVal)

    def setMin(self, minimo):
        self.minimo = minimo
        self.validar()

    def setMax(self, maximo):
        self.maximo = maximo
        self.validar()

    def get(self):
        try:
            valor = float(self.var.get())
            if valor < self.minimo or valor > self.maximo:
                return False
            return valor
        except:
            return False
     
    def validar(self, *args):
        try:
            valor = float(self.var.get())
            if valor < self.minimo or valor > self.maximo:
                self['background'] = 'light coral'
                
            else:
                self['background'] = 'white'
        except:
            self['background'] = 'light coral'
       
#se este arquivo estiver sendo executado como arquivo principal
# uma GUI de teste é apresentada
if __name__ == '__main__':
    root = tk.Tk()
    entry = SmartEntry(root)
    entry.setMin(-11)
    entry.setMax(11)
    entry.pack()
  
    root.mainloop()
