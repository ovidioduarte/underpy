import tkinter as tk
import tkinter.messagebox as msg
from smartentry import SmartEntry


class LaubscherForm(tk.Frame):

    def __init__(self, master, **kw):
        #esta linha informa ao interpretador que esta classe será um frame
        #porém ele será modificado
        tk.Frame.__init__(self, master, **kw)
        campos = [
            'Densidade Material Abatido:',
            'Altura Material abatido:',
            'Profundidade:',
            'Vão livre Mínimo:',
            'Valor do MRMR:'
                ]
        limites = [
            (1,   3,   5),
            (1, 700, 4000),
            (1, 1000, 4000),
            (1, 1000, 2500),
            (10,  50,  90)
                ]
        
        #lista que guradará as entries
        self.entries = []

        f = 'arial 10 bold'
        for i in range(len(campos)):
            l = tk.Label(self, text=campos[i], fg='black', font=f)
            l.grid(row=i, column=0, sticky= 'ne')
            
            #criando as entries
            '''o master da smartentry criada será a instancia deste frame, ou seja,
            laubsherform será o master da classe smartentry. Assim self == laubsherform'''
            entry = SmartEntry(self, initialVal=limites[i][1])

            #configurando os valores minimos e maximos
            entry.setMin(limites[i][0])
            entry.setMax(limites[i][2])

            #geometria e formatação
            entry.configure(justify='center', fg='blue', borderwidth=4, font=f)
            entry.configure(highlightthickness=3, highlightcolor='blue')

            #adicionando a entry criada pra lista de entries
            self.entries.append(entry)
            self.entries[i].grid(row=i, column= 1)

        #Fator
        bFator=tk.Button(self, text='Calcular fator')
        bFator.grid(row=6, column=0)
        bFator['bg'] = 'magenta'
        bFator['activebackground'] = 'blue'
        bFator['command'] = self.calcularFator
        bFator['relief'] = 'raised'
        bFator['borderwidth'] = 5   
        
        self.labelFator = tk.Label(self, text = 'Valor do fator:')
        self.labelFator.configure(fg= 'blue', font = f)
        self.labelFator.grid(row=7, column=0, sticky='e')      
        self.labelValorFator = tk.Label(self, text = '', font = f)
        self.labelValorFator.grid(row=7, column=1, sticky='w')

        #labels que exibirao o valor do angulo de subsidencia
        self.l1 = tk.Label(self, text='Ângulo de abatimento:', font='arial 10 bold')
        self.l1.grid(row=8, column=0, sticky='ne')
        self.l2 = tk.Label(self, text= '', font='arial 10 bold')
        self.l2.grid(row=8, column=1, sticky='nw')  


        
    def calcularFator(self):
        if not self.validar():
            msg.showerror('Erro', 'Existem um ou mais campos com valores incorretos!!\nCorrija os  campos que estiverem vermelho !!!')
            self.labelValorFator['text'] = ''
            return False

        if self.entries[1].get() >= self.entries[2].get():
            msg.showerror('Erro de análise', 'Altura do material abatido não pode \nser maior ou igual a profundidade da mina !!\nCorrija um destes respectivos campos!')
            self.labelValorFator['text'] = ''
            return False
        
        else:
            self.resultado = (self.entries[0].get()/1.5)*(self.entries[1].get()/100)*(self.entries[2].get()/self.entries[3].get())
            if (self.resultado > 100) or (self.resultado < 0.1):
                text = str('Valor do fator é igual a {0}\n\n Tal valor não está '
                           'entre os limites máximo ({1}) e minimo ({2}) !!\n'
                           '\n REVEJA E ALTERE SEUS VALORES DE ENTRADA !')
                
                msg.showerror('Resultado impossível',text.format(round(self.resultado,3),100,0.1))
                self.labelValorFator['text'] = ''
                return False
 
            else:
                self.labelValorFator['text'] = str(round(self.resultado,3))
                return self.resultado
            
        #Calculos.calcularFator(getDensidade(), getAltura())
        #atualizar label
        
    def validar(self):
        """Valida todos os campos fornecidos pelo usuário"""
        
        for e in self.entries:
            if e.get() is False:
                return False
           
        return True

        
##################################           
##################################
if __name__ == '__main__':
    root = tk.Tk()
    form = LaubscherForm(root)
    form.grid(row=0, column=0)
    root.mainloop()
    
