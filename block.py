import tkinter
from tkinter import messagebox
import math
from smartentry import SmartEntry
from laubscherform import LaubscherForm

class Block():

    def __init__(self, master, **kw):
        self.superframe = tkinter.Frame(master)
        self.superframe.grid(row=0, column=0)
        
        self.frame1 = tkinter.Frame(self.superframe)
        self.frame1.grid(row=0, column=0, sticky='nw', padx=10,pady=15)
        self.frame2 = tkinter.Frame(self.superframe)
        self.frame2.grid(row=0, column=1, rowspan=10)
        self.frame3 = tkinter.Frame(self.superframe)
        self.frame3.grid(row=1, column=0, rowspan=4, sticky='nw')
        
        self.frame4 = tkinter.Frame(self.superframe)
        self.frame4.grid(row=2, column=0, rowspan=4, sticky='nw')
        
        c = tkinter.Canvas(self.frame2, bg='white',height=700, width=900)
        c.grid(row=4, column=0)
        
        self.campos = ['Profundidade do Undercut:',
                     'Largura do undercut:',
                     'Ângulo de abatimento:',
                       'Altura do material abatido:',
                       'Altura do Air Gap:'
                     ]
        self.limites =[(50,500,2500),(10,70,600),(1,60,89.99),(10,100,2500),(10,30,2500)]
        self.cria()

        
    def cria(self):
        self.lista = []
        for i in range(len(self.campos)):
            l = tkinter.Label(self.frame1, text= self.campos[i], font='arial 11 bold')
            l.grid(row=i, column=0, sticky='ne')
            e = SmartEntry(self.frame1, self.limites[i][1],self.limites[i][0],self.limites[i][2])
            e.config(borderwidth=4, fg='blue', highlightcolor='blue', justify='center')
            e.config(font='arial 12 bold', highlightthickness=4)
            self.lista.append(e)
            self.lista[i].grid(row=i, column=1)
        bcria = tkinter.Button(self.frame1, text='criar desenho', font='arial 12 bold')
        bcria.config(activebackground='blue', activeforeground='white', borderwidth=4,command=self.desenho)
        bcria.grid(row=6, column=1)
        bcalcula = tkinter.Button(self.frame1, text='calcular Volume', font='arial 12 bold')
        bcalcula.config(activebackground='blue', activeforeground='white', bd=4,command=self.calcula)
        bcalcula.grid(row=6, column=2)
        

    def validar(self):
        for l in self.lista:
            if l.get() is False:
                texto = 'Um dos seguintes erros ocorreu\n\n1) Valor acima ou abaixo do permitido em algum dos campos\n'
                texto += '2) Caracter inválido em algum dos campos\n\nCorrija os campos que estiverem em vermelho !!'
                m = messagebox.showerror('ERRO', texto)
                return False
        return True

    def desenho(self,*args):
        if self.validar() is False:
            return False

        else:
            self.frame2.destroy()
            self.frame2 = tkinter.Frame(self.superframe)
            self.frame2.grid(row=0, column=1, rowspan=15)
            
            self.frame3.destroy()
            self.frame3 = tkinter.Frame(self.superframe)
            self.frame3.grid(row=1, column=0, sticky='nw')

            self.frame4.destroy()
            self.frame4 = tkinter.Frame(self.superframe)
            self.frame4.grid(row=2, column=0, sticky='nw')
            
            c = tkinter.Canvas(self.frame2, bg='aqua',height=700, width=900)
            c.configure(borderwidth=3, highlightcolor='black',highlightbackground='black')
            c.grid(row=0, column=0, ipady=15)
            h = float(c['height'])
            w = float(c['width'])

            #profundidade da mina
            p = (h) * (self.lista[0].get()/self.limites[0][2])

            #definindo os pontos dos iniciais e finais da coordenada de x
            px1 = (w - self.lista[1].get())/2
            px2 = (w + self.lista[1].get())/2
            if (self.lista[3].get() +self.lista[4].get()) >= self.lista[0].get():
                texto = 'A soma do air gap com o material abatido não pode ser \nmaior nem igual a '
                texto += 'profundidade que se encontra o Undercut.\n\nCorrija um destes 3 campos !!'
                m = messagebox.showerror('ERRO DE ENTRADA', texto)
                return False
            
            #transforma a coordenada y do material pra uma forma de coordenada relativa
            yMat = (self.lista[3].get()/self.lista[0].get() * p)
            yMat = (p-yMat)
            
            yAir = ((self.lista[3].get() + self.lista[4].get())/self.lista[0].get() * p)
            yAir = (p-yAir)

            
            #deslocamento da linha em relação ao ponto final do undercut (d)
            # d = profundidade/ tangente do angulo de abatimento
            den = round(math.tan(math.radians(self.lista[2].get())),5)
            
            d = self.lista[0].get()/den
            #nos valores de x finais das linhas deve se fazer operações com as estes valores
            #para ajustar a escala
            xfL1 = (px1)- d
            xfL2 = px2 + d
            #material abatido
            c.create_polygon(px1, p, px2, p, xfL2,0,xfL1,0, fill='orange')
            c.create_rectangle(px1, p, px2,(p+15) , fill='white')
            c.create_rectangle(px1, p, px2,yMat , fill='green')
            c.create_rectangle(px1, yMat, px2,yAir , fill='yellow') 
            
            self.D = 2*d + self.lista[1].get()
            self.D = float(self.D)
            print(self.D)
           
            c.create_line(px1, p,xfL1,0, width=2)
            c.create_line(px2, p,xfL2,0, width=2)

            cores = ['orange', 'green', 'yellow', 'white']
            campos = ['Material abatido', 'Minério abatido','Air Gap','Undercut']
            for c in range(len(cores)):
                l = tkinter.Label(self.frame3, text='', bg=cores[c], width=6)
                l.grid(row=c, column=0)
                l2 = tkinter.Label(self.frame3, text=campos[c])
                l2.grid(row=c, column=1)

    def calcula(self):
        if self.validar() is False:
            return False

        else:
            self.desenho()
            pi = math.pi
            h = self.lista[0].get()
            R = (self.D/2)
            r = (self.lista[1].get()/2)
            volume = ((pi*(self.lista[0].get()))/3)*((R**2)+(R * r)+(r**2))
            print((math.pi*(self.lista[0].get()))/3)
            #volume =volume*
            # se volume der 1000 o volume sera 10³ assim o seu expoente será 3
            expoente = math.log10(volume) 
            expoente =int(expoente)
            #essa linha transforma um valor muito grande
            volume = volume /(10**(expoente))
            volume = str(round(volume,3)) +'*10 ^ '+ str(expoente)
            volume = str(volume) + '  m³'
            
            area = pi * ((R/2)**2)
            area = str(round(area,3)) + ' m²'
            print(area)
            R = str(round(R,3)) + ' m'
            print(R)

            resultados = [('Volume da região abatida: ',volume),
                           ('Raio da área superficial de influência: ',R),
                           ('Área superficial da região Abatida: ',area)                        
                           ]
            i=0
            for i in range(len(resultados)):
                l1 = tkinter.Label(self.frame4, text=resultados[i][0], font='arial 10 bold')
                l1.grid(row=i, column=0, sticky='ne')
                
                l2 = tkinter.Label(self.frame4, text=resultados[i][1], font='arial 10 bold')
                l2.grid(row=i, column=1, sticky='nw')
                i+=1


class Sublevel():

    def __init__(self, master, *args):
        self.superframe = tkinter.Frame(master)
        self.superframe.grid(row=0, column=0)
        
        self.frame1 = tkinter.Frame(self.superframe)
        self.frame1.grid(row=0, column=0, sticky='nw', padx=10,pady=15)
        self.frame2 = tkinter.Frame(self.superframe)
        self.frame2.grid(row=0, column=1, rowspan=10)
        self.frame3 = tkinter.Frame(self.superframe)
        self.frame3.grid(row=1, column=0, rowspan=4, sticky='nw')
        
        self.frame4 = tkinter.Frame(self.superframe)
        self.frame4.grid(row=2, column=0, rowspan=4, sticky='nw')
        
        c = tkinter.Canvas(self.frame2, bg='white',height=700, width=900)
        c.grid(row=4, column=0)


class Caving():

    def __init__(self, master, *args):
        self.superframe=tkinter.Frame(master)
        self.superframe.grid(row=0, column=0)

        self.frame1 = tkinter.Frame(self.superframe)
        self.frame1.grid(row=0,column=0, pady=15)
        self.frame2 = tkinter.Frame(self.superframe)
        self.frame2.grid(row=1,column=0)
        self.var = tkinter.IntVar()
        radios = ['Block Caving', 'Sublevel Caving']
        lista = []

        for i in range(len(radios)):
            print(self.var)
            r = tkinter.Radiobutton(self.frame1, text=radios[i], value=i, variable=self.var)
            r.config(font='arial 12 bold')
            lista.append(r)
            
            lista[i].grid(row=0, column=i)
        
        lista[0]['command'] =  self.block
        lista[1]['command'] =  self.sublevel
        lista[0].invoke()


    def block(self, *args):
        self.frame2.destroy()
        self.frame2 = tkinter.Frame(self.superframe)
        self.frame2.grid(row=1,column=0)
        b = Block(self.frame2)

    def sublevel( self, *args):
        self.frame2.destroy()
        self.frame2 = tkinter.Frame(self.superframe)
        self.frame2.grid(row=1,column=0)
        s = Sublevel(self.frame2)            
                

if __name__ == '__main__':
    root = tkinter.Tk()
    c = Caving(root)
    root.mainloop()
         
