#método 2

import tkinter as tk
from smartentry import SmartEntry
import math
import tkinter.messagebox as msg

'''
    phi_b= angulo de quebra #o que deve ser calculado
    
    phi_p2= Inclinação do novo plano de falha
    z2=nova profundidade da fenda de tração
    alpha= mergulho da superfície do solo
    phi_0=mergulho do minério
    h2= profundidade do nivel lavrado atual
    '''

class Numerico(tk.Frame):
    def __init__(self, master, **kw):
        #inicializando o construdor de um frame
        tk.Frame.__init__(self, master, **kw)
      
        campos = [
            'Inclinação do novo plano de falha:',
            'Nova profundidade da fenda de tração:',
            'Mergulho da superfície do solo:',
            'Mergulho do minério:',
            'Profundidade do nível lavrado atual:'

                ]
        self.entradas = []
        limites = [(0,40,89.9),
                        (0,500,1000),
                        (0,10,90),
                        (0.5,30,90),
                        (1,800,2000)
                        ]
        
        f='Arial 12 bold'
        for i in range(len(campos)):
            l = tk.Label(self, text=campos[i], font=f)
            l.grid(row=i, column=0, sticky='ne')
            
       
        for i in range(len(campos)):
            a = SmartEntry(self, initialVal=limites[i][1], width=12)
            a.configure(justify='center', fg='blue', borderwidth=4, font=f)
            a.configure(highlightthickness=3, highlightcolor='blue')
            
            a.setMin(limites[i][0])
            a.setMax(limites[i][2])
            
            self.entradas.append(a)
            self.entradas[i].grid(row=i, column=1)

        bcalcular = tk.Button(self, text='CALCULAR', bg='silver')
        
        '''opcoes botao:
            flat, groove, raised, ridge, solid or sunken'''
        
        bcalcular.configure(activebackground= 'red', relief='raised')
        bcalcular.configure(borderwidth=5, font=f, highlightcolor='blue')
        bcalcular.grid(row=7, column=0)
        bcalcular['command'] = self.calcula
        
        
        labelAngulo = tk.Label(self, text='Ângulo de Abatimento:')
        labelAngulo.configure(font=f, fg='black')
        labelAngulo.grid(row=8, column=0, stick='ne')

        self.labelResultado = tk.Label(self, text='', font=f, fg='black')
        self.labelResultado.grid(row= 8, column=1, stick='nw')

    def verifica(self, **args):
        
        for i in self.entradas:
                        
            if i.get() is False:
                texto = 'Um dos seguintes erros ocorreu:\n1) Valor abaixo ou acima do permitido\n'
                texto += '2) Caracter inválido em algum campo de entrada\n\nCorrija o que estiver em vermelho !!'
                msg.showerror('Erro de valor',  texto)
                return False
        
        return True

    def calcula(self):
        if self.verifica() is False:
            self.labelResultado['text'] = ''
            return None

        else:
            phi_p2 = math.radians(self.entradas[0].get())
            z2 = self.entradas[1].get()
            alpha = math.radians(self.entradas[2].get())
            phi_0 = math.radians(self.entradas[3].get())
            h2 = self.entradas[4].get()
            
            v1 = math.tan(phi_p2) #tan de phi_p2
            
            v2 = (z2 * math.sin(phi_p2 - alpha)) #numerador do 2° termo
           
            v3 = h2 * (math.sin(alpha + phi_0)) #numerador do denominador da segunda parte
            v4 = math.sin(phi_0) #denominador do denominador da segunda parte
            v5 = z2 * (math.cos(alpha)) #termo sozinho do denominador               

            if z2 >= h2:
                texto = 'A profundidade da fenda de tração não pode ser maior\n '
                texto += 'nem igual a profundidade do nível lavrado atual !!!\n\n'
                texto += 'Altere um destes dois campos !!!'
                m = msg.showerror("Erro", texto)
                return False

            if (v3/v4) == v5:
                texto = 'O denominador da equação deu igual a zero\n'
                texto += 'Por favor altere um dos seguintes campos:\n\n1) Mergulho do Minério\n'
                texto += '2) Mergulho da superfície do solo\n3) Profundidade do nível lavrado atual\n'
                texto += '4) Nova profundidade da fenda de tração'
                m = msg.showerror('Divisão por zero',texto)
                self.labelResultado['text'] = ''
                return False
            
            v6 = math.cos(phi_p2) * ((v3/v4) - v5)
            
            Vfinal = v1 + v2/v6

            fator = math.degrees(math.atan(Vfinal))
            fator = round(fator,3)
            
            self.labelResultado['text'] = str(fator) + ' graus'
            
        

if __name__=='__main__':
    root = tk.Tk()
    n = Numerico(root)
    n.grid(row=0, column=0)
    root.mainloop()
